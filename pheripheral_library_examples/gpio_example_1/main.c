/* Includes ------------------------------------------------------------------*/

#include "peripheral_library\peripheral_library.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/    
/* Private function prototypes -----------------------------------------------*/
// GPIO initialization function declaration
static void PeriphInit(void);
void Delay(uint32_t delay);
/* Private functions ---------------------------------------------------------*/

/* 
EXAMPLE DESCRIPTION:

This example introduces GPIO initialization on the Milandr platform.
This example demonstrates diode blinking on the board using peripheral library.

To initialize GPIO with help of peripheral library, 
you could use the following function:
) int gpioInit (char port_name, int pin_num, char pin_mode);
Input parameters: - char port_name : 'A' - 'E'
                  - int pin_num : 0-7
                  - char pin_mode : 'I' (input) or 'O' (output)
Example:
Initialization of port B, pin 5 for input:
gpioInit('B', 5, 'I');

Initialization of port C, pin 0 for output:
gpioInit('C', 0, 'O');

Error control is provided by the following syntax:
if (gpioInit('C', 0, 'O') != 0){
  // Error handler
}

PLEASE NOTE: there are no additional function to write or read the port.
To do that, you could use standard SPL functions:
PORT_WriteBit and PORT_ReadInputDataBit.


*/

int main()
{
    // GPIO initialization
    PeriphInit();
    
    // Application main cycle
    while(1){
      PORT_WriteBit(MDR_PORTC, PORT_Pin_0, \
                    !PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_0));
      Delay(1000000);
    }
}

// GPIO initialization function definition
static void PeriphInit( void )
{
  if (gpioInit('C', 0, 'O') != 0){
    // Error handler - exit if error occurs during initialization.
    exit(-1);
  }
}

void Delay (uint32_t value)
{
  while ((value--)!=0);
}


#if (USE_ASSERT_INFO == 1)
void assert_failed(uint32_t file_id, uint32_t line)
{
  while (1)
  {
  }
}
#elif (USE_ASSERT_INFO == 2)
void assert_failed(uint32_t file_id, uint32_t line, const uint8_t* expr)
{
  while (1)
  {
  }
}
#endif /* USE_ASSERT_INFO */

/* END OF FILE main.c */
