/* Includes ------------------------------------------------------------------*/

#include "peripheral_library\peripheral_library.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
    int timer_number = 1;   // Timer number
    int init_counter = 0;   // Timer init counter
    int prescaler = 32000;  // Timer prescaler
    int period = 250/2;     // Timer period
/* Private function prototypes -----------------------------------------------*/
// GPIO initialization function declaration
static void PeriphInit(void);
void Delay(uint32_t delay);
/* Private functions ---------------------------------------------------------*/

/* 
EXAMPLE DESCRIPTION:

This example introduces timer initialization on the Milandr platform.
This example demonstrates diode blinking with 0.5 sec period using timer #1.

For standard timer initialization for count UP from init_counter, using
prescaler and until 'period' value won't be reached, you could use the
following peripheral library function:
) int initTimer (int timer_number, int init_counter, int prescaler, int period);
Input parameters: - int timer_number : number of the timer, 1-3
- int init_counter : timer starting value, 0-0xFFFFFFFF
- int prescaler : timer prescaler, 0 - 0xFFFF
- int period : timer period, 0 - 0xFFFFFFFF

By default, the interrupt of the timer TimerX_IRQHandler (where X in 1-3)
is enabled. After timer initialization, every time when timer counter value
will be equal to 'period' value, the interrupt will be called.
There is no any interrupt priority by default.

*/

int main()
{
    // Common initialization function
    PeriphInit();
    // Application main cycle
    while(1){}
}

// GPIO initialization function definition
static void PeriphInit( void )
{
  // Diode initialization using peripheral library
  if (gpioInit('C', 0, 'O') != 0) exit(-1);
  // Timer initialization example
  if (initTimer(timer_number, init_counter, prescaler, period) != 0) exit(-1);
}

void Delay (uint32_t value)
{
  while ((value--)!=0);
}


#if (USE_ASSERT_INFO == 1)
void assert_failed(uint32_t file_id, uint32_t line)
{
  while (1)
  {
  }
}
#elif (USE_ASSERT_INFO == 2)
void assert_failed(uint32_t file_id, uint32_t line, const uint8_t* expr)
{
  while (1)
  {
  }
}
#endif /* USE_ASSERT_INFO */

/* END OF FILE main.c */
