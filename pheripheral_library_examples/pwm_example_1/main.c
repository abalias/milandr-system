/* Includes ------------------------------------------------------------------*/

#include "peripheral_library\peripheral_library.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
    int timer_number = 3;   // Timer number
    int timer_channel = 3;  // Channel number
/* Private function prototypes -----------------------------------------------*/
// GPIO initialization function declaration
static void PeriphInit(void);
void Delay(uint32_t delay);
/* Private functions ---------------------------------------------------------*/

/* 
EXAMPLE DESCRIPTION:

This example introduces PWM usage on the Milandr platform.
To configure PWM with configurable duty ratio parameter the following
functions are provided:
1) int initPwm(int timer_number, int channel_number);
This function should initialize PWM mode on the specified timer 
and timer channel.
Input parameters: int timer number, int channel_number.
Output parameters: init_status : 0 - successful initialization, 
                                -1 - error occured during initialization.
2) int setPwmDutyRatio(int timer_number, int channel_number, int duty_ratio);
This function let you set the specified duty ratio for the channel.
*/

int main()
{
    // Common initialization function
    PeriphInit();
    // Application main cycle
    while(1){
      if ( getPwmDutyRatio(timer_number, timer_channel) == 0xCFFF)
        setPwmDutyRatio(timer_number, timer_channel, 0x4FFF);
      else
        setPwmDutyRatio(timer_number, timer_channel, 0xCFFF);
      Delay(2000000);
    }
}

// GPIO initialization function definition
static void PeriphInit( void )
{
  initPwm(timer_number, timer_channel);
}

void Delay (uint32_t value)
{
  while ((value--)!=0);
}


#if (USE_ASSERT_INFO == 1)
void assert_failed(uint32_t file_id, uint32_t line)
{
  while (1)
  {
  }
}
#elif (USE_ASSERT_INFO == 2)
void assert_failed(uint32_t file_id, uint32_t line, const uint8_t* expr)
{
  while (1)
  {
  }
}
#endif /* USE_ASSERT_INFO */

/* END OF FILE main.c */
