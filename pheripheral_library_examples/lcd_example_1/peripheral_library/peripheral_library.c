/* Milandr periphery library v. 0.1
Creator: A. Baliasnikov
Date: 3/31/2017
Functions:
gpioInit - GPIO initialization;
timerInit - Timer initialization;
pwmInit - PWM initialization;
canInit - CAN initialization;
uartInit - UART initialization;
dacInit - DAC initialization;
adcInit - ADC initialization;
*/

#include "peripheral_library.h"
#include <.\mlt\font.h>

/* GPIO initialization function
    Input parameters:
        1) Port name:
            - 'A'
            - 'B'
            - ...
            - 'F'
        2) Pin number;
            - 
        3) Initialization mode:
            -input
            -ouput
*/
int gpioInit (char port_name, int pin_num, char pin_mode){
    // Initialization status set to default
    int init_status = 0;
    
    // SPL port initialization structure
    PORT_InitTypeDef PORT_InitStruct;
    
    // Define port name
    MDR_PORT_TypeDef* PORTx;
    int port_clk;
    switch(port_name){
        case 'A' : PORTx = MDR_PORTA; port_clk = RST_CLK_PCLK_PORTA; break;
        case 'B' : PORTx = MDR_PORTB; port_clk = RST_CLK_PCLK_PORTB; break;
        case 'C' : PORTx = MDR_PORTC; port_clk = RST_CLK_PCLK_PORTC; break;
        case 'D' : PORTx = MDR_PORTD; port_clk = RST_CLK_PCLK_PORTD; break;
        case 'E' : PORTx = MDR_PORTE; port_clk = RST_CLK_PCLK_PORTE; break;
        case 'F' : PORTx = MDR_PORTF; port_clk = RST_CLK_PCLK_PORTF; break;
        default : 
            init_status = -1;
            printf("Non existing port name.\nPlease, see the documentation for more info.\n");
            return init_status;
    }
    // Set CLK for the port
    RST_CLK_PCLKcmd( RST_CLK_PCLK_RST_CLK | port_clk, ENABLE );

    // Define pin number for the initialization
    int PIN;
    switch(pin_num){
        case 0 : PIN = PORT_Pin_0; break;
        case 1 : PIN = PORT_Pin_1; break;
        case 2 : PIN = PORT_Pin_2; break;
        case 3 : PIN = PORT_Pin_3; break;
        case 4 : PIN = PORT_Pin_4; break;
        case 5 : PIN = PORT_Pin_5; break;
        case 6 : PIN = PORT_Pin_6; break;
        case 7 : PIN = PORT_Pin_7; break;
        case 8 : PIN = PORT_Pin_8; break;
        case 9 : PIN = PORT_Pin_9; break;
        case 10 : PIN = PORT_Pin_10; break;
        case 11 : PIN = PORT_Pin_11; break;
        case 12 : PIN = PORT_Pin_12; break;
        case 13 : PIN = PORT_Pin_13; break;
        case 14 : PIN = PORT_Pin_14; break;
        case 15 : PIN = PORT_Pin_15; break;
        default :
            init_status = -1;
            printf("Non existing pin number.\nPlease, see the documentation for more info.\n");
            return init_status;
    }

    // Define PIN mode: input or output
    PORT_OE_TypeDef MODE;
    switch(pin_mode){
        case 'I' : MODE = PORT_OE_IN; break;
        case 'O' : MODE = PORT_OE_OUT; break;
        default :
            init_status = -1;
            printf("Non existing pin mode.\nPlease, see the documentation for more info.\n");
            return init_status;
    }

    // Common GPIO settings
    PORT_StructInit(&PORT_InitStruct);
    PORT_InitStruct.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStruct.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStruct.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_InitStruct.PORT_GFEN  = PORT_GFEN_ON;

    // Function GPIO settings
    PORT_InitStruct.PORT_Pin   = PIN;
    PORT_InitStruct.PORT_OE    = MODE;

    // Port initialization    
    PORT_Init( PORTx, &PORT_InitStruct );
    
    return init_status;
}


/* PWM initialization function:
    Configurable parameters:
    1) Timer number;
    2) Channel of the timer;
*/
int initPwm(int timer_number, int channel_number){
    // Initialization status set to default
    int init_status = 0;
    // Initial Q value (half of maximum)
    int INITIAL_PWM_Q = 0xCFFF;
    /* 
        1) Setup the PORT and PIN to correspond timer and channel    
    */
    MDR_PORT_TypeDef* PORTx;
    MDR_TIMER_TypeDef* TIMERx;
    PORT_FUNC_TypeDef FUNC;
    int PIN;
    int port_clk;
    uint32_t timer_clk;
    uint32_t timer_channel;
    IRQn_Type irq;
    switch(timer_number){
        case 1: 
                TIMERx = MDR_TIMER1;
                PORTx = MDR_PORTA;
                FUNC = PORT_FUNC_ALTER;
                switch(channel_number){
                    case 1: PIN = PORT_Pin_1; timer_channel = TIMER_CHANNEL1; TIMERx->CCR1 = INITIAL_PWM_Q; break; // verified PA1 alter - PASSED, PD1 alter - FAILED, others - not supported
                    case 2: PIN = PORT_Pin_3; timer_channel = TIMER_CHANNEL2; TIMERx->CCR2 = INITIAL_PWM_Q; break; // verified PA3 alter - PASSED, others - not supported
                    case 3: PIN = PORT_Pin_5; timer_channel = TIMER_CHANNEL3; TIMERx->CCR3 = INITIAL_PWM_Q; break; // verified PA5 alter - PASSED, others - not supported
                    default:
                            init_status = -1;
                            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
                            return init_status;   
                }
                port_clk = RST_CLK_PCLK_PORTA;
                timer_clk = RST_CLK_PCLK_TIMER1;
                irq = Timer1_IRQn;
                break; 
        case 2: 
                TIMERx = MDR_TIMER2;
                PORTx = MDR_PORTA;
                FUNC = PORT_FUNC_OVERRID;
                switch(channel_number){
                    case 1: PIN = PORT_Pin_1; timer_channel = TIMER_CHANNEL1; TIMERx->CCR1 = INITIAL_PWM_Q; break; // verified PA1 overrid - PASSED, others - not supported
                    case 2: PIN = PORT_Pin_3; timer_channel = TIMER_CHANNEL2; TIMERx->CCR2 = INITIAL_PWM_Q; break;  // verified PA3 overrid - PASSED, others - not supported
                    case 3: PIN = PORT_Pin_5; timer_channel = TIMER_CHANNEL3; TIMERx->CCR3 = INITIAL_PWM_Q; break;  // verified PA5 overrid - PASSED, others - not supported
                    default:
                            init_status = -1;
                            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
                            return init_status;   
                }
                port_clk = RST_CLK_PCLK_PORTA;
                timer_clk = RST_CLK_PCLK_TIMER2;
                irq = Timer2_IRQn;
                break; 
        case 3: 
                TIMERx = MDR_TIMER3;
                PORTx = MDR_PORTB;
                switch(channel_number){
                    case 1: PIN = PORT_Pin_0; FUNC = PORT_FUNC_ALTER; timer_channel = TIMER_CHANNEL1; TIMERx->CCR1 = INITIAL_PWM_Q; break; // verified PB0 alter - PASSED
                    //case 1: PIN = PORT_Pin_2; FUNC = PORT_FUNC_ALTER; timer_channel = TIMER_CHANNEL1; PORTx = MDR_PORTC; port_clk = RST_CLK_PCLK_PORTC; TIMERx->CCR1 = INITIAL_PWM_Q; break; // verified PC2 alter - PASSED, PD0 overrid - FAILED, others - not supported
                    case 2: PIN = PORT_Pin_2; FUNC = PORT_FUNC_ALTER; timer_channel = TIMER_CHANNEL2; TIMERx->CCR2 = INITIAL_PWM_Q; break; // verified PB2 alter - PASSED, verified PD2 overrid - FAILED, others - not supported
                    case 3: PIN = PORT_Pin_5; FUNC = PORT_FUNC_OVERRID; timer_channel = TIMER_CHANNEL3; TIMERx->CCR3 = INITIAL_PWM_Q; break; // verified PB5 overrid - PASSED, others - not supported
                    default:
                            init_status = -1;
                            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
                            return init_status;   
                }
                port_clk = RST_CLK_PCLK_PORTB;
                timer_clk = RST_CLK_PCLK_TIMER3;
                irq = Timer3_IRQn;
                break; 
        default : 
            init_status = -1;
            printf("Non existing timer number.\nPlease, see the documentation for more info.\n");
            return init_status;
    }
    
    // Set CLK for the port
    RST_CLK_PCLKcmd( RST_CLK_PCLK_RST_CLK | port_clk, ENABLE );
    
    // PWM pin initialization
    PORT_InitTypeDef PWM_structInit;
    // Configurable parameters
    PWM_structInit.PORT_FUNC = FUNC;
    PWM_structInit.PORT_Pin = PIN;
    // Common parameters
    PWM_structInit.PORT_GFEN = PORT_GFEN_OFF;
    PWM_structInit.PORT_MODE = PORT_MODE_DIGITAL;
    PWM_structInit.PORT_OE = PORT_OE_OUT;
    PWM_structInit.PORT_PD = PORT_PD_DRIVER; 
    PWM_structInit.PORT_PD_SHM = PORT_PD_SHM_OFF;
    PWM_structInit.PORT_PULL_DOWN = PORT_PULL_DOWN_OFF;
    PWM_structInit.PORT_PULL_UP = PORT_PULL_UP_OFF;
    PWM_structInit.PORT_SPEED = PORT_SPEED_MAXFAST;
    // PORTx initialization
    PORT_Init(PORTx, &PWM_structInit);
    
    // Enable CLK
    RST_CLK_PCLKcmd(timer_clk, ENABLE);
    // No CLK division
    TIMER_BRGInit(TIMERx, TIMER_HCLKdiv1);
                     
    // Timer init structure
    TIMER_CntInitTypeDef TIMER_CntInitStruct;
    // Timer channel init structure
    TIMER_ChnInitTypeDef timerPWM_channelStructInit;
    // Timer channel out init structure
    TIMER_ChnOutInitTypeDef timerPWM_channelOUTPWMStructInit;
    
    // Initialize timer params with defaults
    TIMER_CntStructInit(&TIMER_CntInitStruct);
    // Timer main params initialization
    // Max period without prescaler
    TIMER_CntInitStruct.TIMER_Period = 0xFFFF;
    TIMER_CntInitStruct.TIMER_Prescaler = 0;
    TIMER_CntInit( TIMERx, &TIMER_CntInitStruct );
    
    // Timer channel initialization
    TIMER_ChnStructInit(&timerPWM_channelStructInit);
    // As per documentation REF_format6 should be in use, but format 3 works as 6
    //timerPWM_channelStructInit.TIMER_CH_REF_Format = TIMER_CH_REF_Format6;
    timerPWM_channelStructInit.TIMER_CH_Number = timer_channel;
    timerPWM_channelStructInit.TIMER_CH_REF_Format = TIMER_CH_REF_Format3;
    timerPWM_channelStructInit.TIMER_CH_CCR1_Ena = ENABLE;
    timerPWM_channelStructInit.TIMER_CH_CCR_UpdateMode = TIMER_CH_CCR_Update_On_CNT_eq_0;
    timerPWM_channelStructInit.TIMER_CH_Mode = TIMER_CH_MODE_PWM;
    TIMER_ChnInit(TIMERx, &timerPWM_channelStructInit); 
    
    // Timer output channel initialization
    TIMER_ChnOutStructInit(&timerPWM_channelOUTPWMStructInit);
    timerPWM_channelOUTPWMStructInit.TIMER_CH_DirOut_Mode = TIMER_CH_OutMode_Output;
    timerPWM_channelOUTPWMStructInit.TIMER_CH_DirOut_Source = TIMER_CH_OutSrc_REF;
    timerPWM_channelOUTPWMStructInit.TIMER_CH_Number = timer_channel;	
    TIMER_ChnOutInit(TIMERx, &timerPWM_channelOUTPWMStructInit); 
    
    // Enable interrupt
    TIMER_ITConfig( TIMERx, TIMER_STATUS_CNT_ARR, ENABLE );
    NVIC_EnableIRQ( irq ); 
    
    // Start Timer
    TIMER_Cmd( TIMERx, ENABLE );
    
    return init_status;
}

/* PWM set duty ratio (Q) function:
    Configurable parameters:
    1) Timer number;
    2) Channel of the timer;
    3) Duty ratio: from 0x000 to 0xFFFF;
*/
int setPwmDutyRatio(int timer_number, int channel_number, int duty_ratio){
    // Initialization status set to default
    int init_status = 0;
    MDR_TIMER_TypeDef* TIMERx;
    switch(timer_number){
      case 1:  
        TIMERx = MDR_TIMER1; 
        switch(channel_number){
          case 1: TIMERx->CCR1 = duty_ratio; break;
          case 2: TIMERx->CCR2 = duty_ratio; break;
          case 3: TIMERx->CCR3 = duty_ratio; break;
          default:
            init_status = -1;
            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
            return init_status; 
        }
        break;
      case 2:  
        TIMERx = MDR_TIMER2; 
        switch(channel_number){
          case 1: TIMERx->CCR1 = duty_ratio; break;
          case 2: TIMERx->CCR2 = duty_ratio; break;
          case 3: TIMERx->CCR3 = duty_ratio; break;
          default:
            init_status = -1;
            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
            return init_status; 
        }
        break;
      case 3:  
        TIMERx = MDR_TIMER3; 
        switch(channel_number){
          case 1: TIMERx->CCR1 = duty_ratio; break;
          case 2: TIMERx->CCR2 = duty_ratio; break;
          case 3: TIMERx->CCR3 = duty_ratio; break;
          default:
            init_status = -1;
            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
            return init_status; 
        }
        break;
      default : 
            init_status = -1;
            printf("Non existing timer number.\nPlease, see the documentation for more info.\n");
            return init_status;
    }
        
    return init_status;
}

/* PWM get duty ratio (Q) function:
    Configurable parameters:
    1) Timer number;
    2) Channel of the timer;
*/
int getPwmDutyRatio(int timer_number, int channel_number){
    // Initialization status set to default
    int init_status = 0;
    MDR_TIMER_TypeDef* TIMERx;
    switch(timer_number){
      case 1:  
        TIMERx = MDR_TIMER1; 
        switch(channel_number){
          case 1: return TIMERx->CCR1;
          case 2: return TIMERx->CCR2;
          case 3: return TIMERx->CCR3;
          default:
            init_status = -1;
            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
            return init_status; 
        }
      case 2:  
        TIMERx = MDR_TIMER2; 
        switch(channel_number){
          case 1: return TIMERx->CCR1;
          case 2: return TIMERx->CCR2;
          case 3: return TIMERx->CCR3;
          default:
            init_status = -1;
            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
            return init_status; 
        }
      case 3:  
        TIMERx = MDR_TIMER3; 
        switch(channel_number){
          case 1: return TIMERx->CCR1;
          case 2: return TIMERx->CCR2;
          case 3: return TIMERx->CCR3;
          default:
            init_status = -1;
            printf("Non existing channel number.\nPlease, see the documentation for more info.\n");
            return init_status; 
        }
      default : 
            init_status = -1;
            printf("Non existing timer number.\nPlease, see the documentation for more info.\n");
            return init_status;
    }
}

/* Timer initialization function.

Native frequency is 8 MHz = 125 nS.
PLEASE NOTE: this is only standard timer initialization.

Input parameters:
  - Timer number
  - Init counter
  - Timer prescaler
  - Timer period

*/
int initTimer (int timer_number, int init_counter, int prescaler, int period) 
{ 
    // Initialization status set to default
    int init_status = 0;
    MDR_TIMER_TypeDef* TIMERx;
    uint32_t timer_clk;
    IRQn_Type irq;
    
    // Define timer number for initialization
    switch(timer_number){
      case 1: TIMERx = MDR_TIMER1; timer_clk = RST_CLK_PCLK_TIMER1; irq = Timer1_IRQn; break;
      case 2: TIMERx = MDR_TIMER2; timer_clk = RST_CLK_PCLK_TIMER2; irq = Timer2_IRQn; break;
      case 3: TIMERx = MDR_TIMER3; timer_clk = RST_CLK_PCLK_TIMER3; irq = Timer3_IRQn; break;
      default : 
            init_status = -1;
            printf("Non existing timer number.\nPlease, see the documentation for more info.\n");
            return init_status;
    }
    
    // Timer init struct
    TIMER_CntInitTypeDef timer_structInit;
    
    // Enable timer CLK
    RST_CLK_PCLKcmd(timer_clk, ENABLE); 
    TIMER_BRGInit(TIMERx, TIMER_HCLKdiv1);
    
    // Timer structure filling
    timer_structInit.TIMER_ARR_UpdateMode = TIMER_ARR_Update_Immediately;
    timer_structInit.TIMER_BRK_Polarity = TIMER_BRKPolarity_NonInverted; 
    timer_structInit.TIMER_CounterDirection = TIMER_CntDir_Up;
    timer_structInit.TIMER_CounterMode = TIMER_CntMode_ClkFixedDir;
    timer_structInit.TIMER_ETR_FilterConf = TIMER_Filter_1FF_at_TIMER_CLK;
    timer_structInit.TIMER_ETR_Polarity = TIMER_ETRPolarity_NonInverted;
    timer_structInit.TIMER_ETR_Prescaler = TIMER_ETR_Prescaler_None;
    timer_structInit.TIMER_EventSource = TIMER_EvSrc_TM2;
    timer_structInit.TIMER_FilterSampling = TIMER_FDTS_TIMER_CLK_div_1;
    
    // Timer main params
    timer_structInit.TIMER_IniCounter = init_counter;
    timer_structInit.TIMER_Period = period;
    if (prescaler == 0) timer_structInit.TIMER_Prescaler = 0;
    else timer_structInit.TIMER_Prescaler = prescaler - 1; 
    
    // Timer 2 initialization
    TIMER_CntInit(TIMERx, &timer_structInit);
    // Timer 2 interrupt enable
    TIMER_ITConfig(TIMERx, TIMER_STATUS_CNT_ARR, ENABLE);	
    NVIC_EnableIRQ(irq);
    // Start Timer 2
    TIMER_Cmd(TIMERx, ENABLE);
    
    return init_status;
}

/* 
    LCD initialization function
*/
int lcdInit(void){
    // Initialization status set to default
    int init_status = 0;
    // LCD buffer
    uint8_t buffer[1024] = {0};
    
    // LCD GPIO initialization
    // PC0-PC1
    gpioInit ('C', 0, 'O');
    gpioInit ('C', 1, 'O');
    // PB7-PB10
    gpioInit ('B', 7, 'O');
    gpioInit ('B', 8, 'O');
    gpioInit ('B', 9, 'O');
    gpioInit ('B', 10, 'O');
    // PA0-PA5
    gpioInit ('A', 0, 'O');
    gpioInit ('A', 1, 'O');
    gpioInit ('A', 2, 'O');
    gpioInit ('A', 3, 'O');
    gpioInit ('A', 4, 'O');
    gpioInit ('A', 5, 'O');
    // PF2-PF3
    gpioInit ('F', 2, 'O');
    gpioInit ('F', 3, 'O');
    
    // LCD initialization
    // LCD Display initialization
    Set_Res_Pin;
    LcdInit(); //LCD module init
    LcdClearChip(1); //Clear chip 1
    LcdClearChip(2); //Clear chip 2
    DispOn(1);
    DispOn(2);
    
    // Clear the display
    for(int i=0;i<1024;i++)
          buffer[i] = 0x00;
    LcdWriteBuffer (buffer);
    
    return init_status;
}

/* 
    ASCII table function for LCD symbols
*/
uint8_t* ascii(char symbol)
{
    uint8_t* ch_ptr;
    switch (symbol){
    case '0': ch_ptr=dig_0; break;
    case '1': ch_ptr=dig_1; break;
    case '2': ch_ptr=dig_2; break;
    case '3': ch_ptr=dig_3; break;
    case '4': ch_ptr=dig_4; break;
    case '5': ch_ptr=dig_5; break;
    case '6': ch_ptr=dig_6; break;
    case '7': ch_ptr=dig_7; break;
    case '8': ch_ptr=dig_8; break;
    case '9': ch_ptr=dig_9; break;
    case 'N': ch_ptr=lat_N; break;
    case 'e': ch_ptr=lat_e; break;
    case 'w': ch_ptr=lat_w; break;
    case 'g': ch_ptr=lat_g; break;
    case 'a': ch_ptr=lat_a; break;
    case 'm': ch_ptr=lat_m; break;
    case 'l': ch_ptr=lat_l; break;
    case 'R': ch_ptr=lat_R; break;
    case 'c': ch_ptr=lat_c; break;
    case 'o': ch_ptr=lat_o; break;
    case 'r': ch_ptr=lat_r; break;
    case 'd': ch_ptr=lat_d; break;
    case 's': ch_ptr=lat_s; break;
    case 'E': ch_ptr=lat_E; break;
    case 'x': ch_ptr=lat_x; break;
    case 'i': ch_ptr=lat_i; break;
    case 't': ch_ptr=lat_t; break;
    case 'H': ch_ptr=lat_H; break;
    case 'V': ch_ptr=lat_V; break;
    case 'k': ch_ptr=lat_k; break;
    case 'z': ch_ptr=lat_z; break;
    case 'S': ch_ptr=lat_S; break;
    case 'p': ch_ptr=lat_p; break;
    case '.': ch_ptr=sym_pt;break;
    default: ch_ptr=sym_sp;break;
    }
        return(ch_ptr);
}

