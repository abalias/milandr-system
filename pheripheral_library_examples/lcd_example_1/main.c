/* Includes ------------------------------------------------------------------*/

#include "peripheral_library\peripheral_library.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/    
/* Private function prototypes -----------------------------------------------*/
// GPIO initialization function declaration
static void PeriphInit(void);
void Delay(uint32_t delay);
/* Private functions ---------------------------------------------------------*/

/* 
EXAMPLE DESCRIPTION:

This example introduces LCD initialization on the Milandr platform.
This example demonstrates how to print a simple string on the screen.
As the result: "Hello world" should appear on LCD.

LCD initialization is provided by:
int lcdInit(void) function that returns init_status:
0 - successful initialization,
-1 - error occured during initialization.

To work with LCD, the following functions are available:
void LcdPutChar (uint8_t* array, int Xpos, int Ypos);
void LcdPutString (uint8_t** array, int Ypos);
int LcdScrollString (uint8_t** array, int Ypos, int size,int cnt);
void LcdPutImage (uint8_t* array, int Ypos1, int Xpos1,int Ypos2, int Xpos2);
void LcdWriteBuffer (uint8_t *buffer);

Description will be updated in the future releases of peripheral library.
*/

int main()
{
    // Periphery initialization
    PeriphInit();
    
    // Write a string into LCD
    //uint8_t *array[16] = {ascii('H'), ascii('e'), ascii('l'), ascii('l'), ascii('o'), \
                         ascii(' '), ascii('w'), ascii('o'), ascii('r'), ascii('l'), ascii('d'), ascii('!'),\
                         ascii(' '), ascii(' '),  ascii(' '),  ascii(' ')};
    //LcdPutString (array, 2);
    uint8_t array[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    
    LcdPutRow(array, 12);
    LcdPutRow(array, 45);
    LcdPutRow(array, 69);
    // Application main cycle
    while(1){}
}

// Periphery initialization function definition
static void PeriphInit( void )
{
    // LCD initialization
    if (lcdInit() != 0) exit(-1);
}

void Delay (uint32_t value)
{
  while ((value--)!=0);
}


#if (USE_ASSERT_INFO == 1)
void assert_failed(uint32_t file_id, uint32_t line)
{
  while (1)
  {
  }
}
#elif (USE_ASSERT_INFO == 2)
void assert_failed(uint32_t file_id, uint32_t line, const uint8_t* expr)
{
  while (1)
  {
  }
}
#endif /* USE_ASSERT_INFO */

/* END OF FILE main.c */
