/* Includes ------------------------------------------------------------------*/

#include ".\peripheral_library\peripheral_library.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define INITIAL_PWM_Q 0x1FFF

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
    // Engine speed
    float speed = 0;
    int rem = 0; 
    int rem_prev = 0;
    int spd = 0;
    int spd_prev = 0;
    float K_del = 1.2;
    // FID control flag
    int flag = 0;
    // FID output counter
    int count = 0;
    // Buttons flag
    int b_flag = 0;
    // Lcd flag
    int lcd_flag = 0;
    // Buffer
    static uint8_t buffer[8] = {0};
    // Clean Buffer
    static uint8_t cleanRow[8] = {0};
    // LCD period
    static int k = 0;
    // PID variables
    float Kp = 1;
    float Ki = 1;
    float Kd = 1;
    uint16_t setpoint = INITIAL_PWM_Q;
    int pErr = 0;
    int sum = 0;
    
    int K_time = 1191;
    
/* Private function prototypes -----------------------------------------------*/
// GPIO initialization function declaration
void PeriphInit(void);
// UART initialization function declaration
void UART_init(void);
// Fill one lcd column
void lcd_step(void);
// Return byte from int for LCD filling
uint8_t to_byte(int rem);
// Set pwm Q with respect to min/max available value
void setPwmQ(int Q);
// Execute UART command
void execute_uart_command(uint16_t command);
/* Private functions ---------------------------------------------------------*/

int main()
{
    // Periphery initialization
    PeriphInit();
    
    // Application main cycle
    while(1){
      // Buttons scanning
      if (b_flag == 1){
          b_flag = 0;
          //PWQ control
          if (PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_5) == 0)
            setPwmQ(MDR_TIMER3->CCR1 + 1000);
          else if (PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_1) == 0)
            setPwmQ(MDR_TIMER3->CCR1 - 1000);
          //LCD control
          if (PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_6) == 0) {
            if (MDR_TIMER2->ARR < 128)
              MDR_TIMER2->ARR *= 2;
          } else if (PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_3) == 0) {
            if (MDR_TIMER2->ARR > 4)
              MDR_TIMER2->ARR /= 2;
          }
      }
      if (lcd_flag == 1){    
        lcd_step();
        lcd_flag = 0;
      }
    }
    
}

// GPIO initialization function definition
static void PeriphInit( void )
{
    // LCD Display initialization
    lcdInit();
    // GPIO port initialization structure
    PORT_InitTypeDef PORT_InitStruct;
    
    // Enable CLK on peripheral devices
    RST_CLK_PCLKcmd( RST_CLK_PCLK_RST_CLK | RST_CLK_PCLK_PORTC | RST_CLK_PCLK_PORTF 
                    | RST_CLK_PCLK_TIMER1 | RST_CLK_PCLK_TIMER2
                    | RST_CLK_PCLK_PORTB | RST_CLK_PCLK_PORTE 
                    | RST_CLK_PCLK_TIMER3 | RST_CLK_PCLK_PORTA, ENABLE );
    
    // PB2 - X26:15: FID input
    gpioInit ('B', 2, 'I');
    // 'DOWN' button initialization on PE1: Move speed DOWN
    gpioInit ('E', 1, 'I');
    // 'UP' button initialization on PB5: Move speed UP
    gpioInit ('B', 5, 'I');
    // 'LEFT' button initialization - LCD timing up PE3
    gpioInit ('E', 3, 'I');
    // 'RIGHT' button initialization - LCD timing down PB6
    gpioInit ('B', 6, 'I');
    // 'SELECT' button initialization on PC2 - X26:26: Sign of PWM  
    gpioInit ('C', 2, 'I');
      
    // PF0-PF1 (UART2 rx/tx)
    PORT_InitStruct.PORT_Pin                = PORT_Pin_0;
    PORT_InitStruct.PORT_OE                 = PORT_OE_IN;
    PORT_InitStruct.PORT_PULL_UP            = PORT_PULL_UP_OFF;
    PORT_InitStruct.PORT_PULL_DOWN          = PORT_PULL_DOWN_OFF;
    PORT_InitStruct.PORT_PD_SHM             = PORT_PD_SHM_OFF;
    PORT_InitStruct.PORT_PD                 = PORT_PD_DRIVER;
    PORT_InitStruct.PORT_GFEN               = PORT_GFEN_OFF;
    PORT_InitStruct.PORT_FUNC               = PORT_FUNC_OVERRID;
    PORT_InitStruct.PORT_SPEED              = PORT_SPEED_MAXFAST;
    PORT_InitStruct.PORT_MODE               = PORT_MODE_DIGITAL;
    PORT_Init(MDR_PORTF, &PORT_InitStruct);
    PORT_InitStruct.PORT_Pin                = PORT_Pin_1;
    PORT_InitStruct.PORT_OE                 = PORT_OE_OUT;
    PORT_Init(MDR_PORTF, &PORT_InitStruct);
    
    // UART initialization
    UART_init();
    // Button check timer initializtion
    initTimer (2, 0, 32000, 32);
    // PWM timer initialization
    initPwm(3, 1);
    // FID sensor initialization
    initTimer (1, 0, 8, 40);
    
    // Set priority for timings
    NVIC_SetPriorityGrouping( 3 );
    NVIC_SetPriority ( Timer1_IRQn, 1 );
    NVIC_SetPriority ( Timer2_IRQn, 0 );
}

void UART_init(void)
{
    UART_InitTypeDef uart_user_ini;

    RST_CLK_PCLKcmd(RST_CLK_PCLK_UART2, ENABLE);
    UART_BRGInit(MDR_UART2, UART_HCLKdiv1);
    NVIC_EnableIRQ(UART2_IRQn);

    uart_user_ini.UART_BaudRate = 9600;
    uart_user_ini.UART_FIFOMode = UART_FIFO_OFF;
    uart_user_ini.UART_HardwareFlowControl = UART_HardwareFlowControl_TXE | UART_HardwareFlowControl_RXE;
    uart_user_ini.UART_Parity = UART_Parity_No;
    uart_user_ini.UART_StopBits = UART_StopBits1;
    uart_user_ini.UART_WordLength = UART_WordLength8b;

    UART_Init(MDR_UART2, &uart_user_ini);
    UART_ITConfig(MDR_UART2, UART_IT_RX, ENABLE);
    UART_Cmd(MDR_UART2, ENABLE);
}

// Return low byte from int for LCD filling
uint8_t to_low_byte(int rem){
  switch(rem){
      case 0: return 0xFF;
      case 1: return 0xFE;
      case 2: return 0xFC;
      case 3: return 0xF8;
      case 4: return 0xF0;
      case 5: return 0xE0;
      case 6: return 0xC0;
      case 7: return 0x80;
      default: return 0x55; // error
  }
}

// Return high byte from int for LCD filling
uint8_t to_high_byte(int rem){
  switch(rem){
      case 0: return 0x01;
      case 1: return 0x03;
      case 2: return 0x07;
      case 3: return 0x0F;
      case 4: return 0x1F;
      case 5: return 0x3F;
      case 6: return 0x7F;
      case 7: return 0xFF;
      default: return 0x55; // error
  }
}

// Return byte from int for LCD filling
uint8_t to_byte(int rem){
  switch(rem){
      case 0: return 0x80;
      case 1: return 0x40;
      case 2: return 0x20;
      case 3: return 0x10;
      case 4: return 0x08;
      case 5: return 0x04;
      case 6: return 0x02;
      case 7: return 0x01;
      default: return 0x55; // error
  }
}

void fill_within(int bit_prev, int bit_cur){
  int sum = 0;
  int tmp = 1;
  if ( bit_prev < bit_cur ){
    for(int i = 7 - bit_prev ; i >= 7 - bit_cur; i--){
        for(int j = 0; j < i; j++) tmp *= 2;
        sum += tmp;
        tmp = 1;
    }
  }
  else{
    for(int i = 7 - bit_prev ; i <= 7 - bit_cur; i++){
        for(int j = 0; j < i; j++) tmp *= 2;
        sum += tmp;
        tmp = 1;
    }
  }
  buffer[7 - rem] = sum;
}

int speed_tmp = 0;
// Fill LCD column
void lcd_step(void){
  int bit_number_high, bit_number_low;
  int i;
  
  // Remember previous step
    speed_tmp = speed / K_del;
    rem_prev = rem;
    spd_prev = spd;
    
    // New values
    rem = (speed_tmp / 8);
    spd = (speed_tmp - rem*8);
    
    // Evaluate bit_low and bit_high
    int bit_1 = 63 - (rem*8 + spd);
    int bit_2 = 63 - (rem_prev*8 + spd_prev);
    if (bit_1 >= bit_2){
        bit_number_high = bit_1;
        bit_number_low = bit_2;
    }
    else{
        bit_number_high = bit_2;
        bit_number_low = bit_1;
    }
    
    // ���� �� ����� �� ������� ������ ����� � ��������� ������ 2 ���, �� ������������ �����
    if ( rem == rem_prev){
       if( spd == spd_prev ) buffer[7 - rem] = (uint8_t)to_byte(spd);
       else fill_within(spd_prev, spd);
    }
    else{
      // Fill buffer low byte
      buffer[8 - (8-bit_number_low/8)] = to_low_byte(8 - (8*(bit_number_low/8 + 1) - bit_number_low));
      // Fill buffer high byte
      buffer[8 - (8-bit_number_high/8)] = to_high_byte(8 - (8*(bit_number_high/8 + 1) - bit_number_high));
      // Fill between bytes
      for(int i = bit_number_low/8+1; i < bit_number_high/8; i++){
          buffer[i] = 0xFF; 
      }
    }
    
    // Write setpoint to LCD
    uint8_t setpoint_pixel = (MDR_TIMER3->CCR1 / K_time / K_del);
    uint8_t setpoint_rem = (setpoint_pixel/8);
    uint8_t setpoint_spd = (setpoint_pixel - setpoint_rem*8);
    buffer[7 - setpoint_rem] |= to_byte(setpoint_spd);
    
    // Write buffer to LCD
    LcdPutRow (buffer,k);
    memset(buffer,0,8);
    
    int row = k;
    for (i = 0; i < 5; ++i) {
      row++;
      if (row > 127) row = 0;
      LcdPutRow (cleanRow,row);
    }
    k++;
    if (k > 127) k = 0;
}

void setPwmQ(int Q) 
{
    if (Q < 0x1F) Q = 0x1F;									
    else if (Q > 0xFFC0) Q = 0xFFC0;
    setPwmDutyRatio(3, 1, Q);
}

void execute_uart_command(uint16_t command)
{
  switch(command){
  case 0xFFC1:
    k = 0;
  default:
    break;
  }
  
  return;
}

#if (USE_ASSERT_INFO == 1)
void assert_failed(uint32_t file_id, uint32_t line)
{
  while (1)
  {
  }
}
#elif (USE_ASSERT_INFO == 2)
void assert_failed(uint32_t file_id, uint32_t line, const uint8_t* expr)
{
  while (1)
  {
  }
}
#endif /* USE_ASSERT_INFO */

/* END OF FILE main.c */
