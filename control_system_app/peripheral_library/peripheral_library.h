/* Milandr periphery library v. 0.1
Creator: A. Baliasnikov
Date: 3/31/2017
Functions:
gpioInit - GPIO initialization;
timerInit - Timer initialization;
pwmInit - PWM initialization;
canInit - CAN initialization;
uartInit - UART initialization;
dacInit - DAC initialization;
adcInit - ADC initialization;
*/

#include "MDR32Fx.h" 
#include "MDR32F9Qx_config.h" 
#include "MDR32F9Qx_port.h" 
#include "MDR32F9Qx_rst_clk.h" 
#include "MDR32F9Qx_timer.h"
#include "MDR32F9Qx_uart.h"
#include <.\mlt\mlt_lcd.h>
#include <stdio.h>

/* Periphery initialization status:
    0 - initialization completed successfully;
    -1 - initialization failed.
*/

/* GPIO initialization function
    Input parameters:
        1) Port name:
            - PORTA
            - PORTB
            - ...
            - PORTF
        2) Pin number;
            - 0...15
        3) Initialization mode:
            -input
            -ouput
*/
int gpioInit (char port_name, int pin_num, char pin_mode);

/* PWM initialization function:
    Configurable parameters:
    1) Timer number;
    2) Channel of the timer;
*/
int initPwm(int timer_number, int channel_number);

/* PWM set duty ratio (Q) function:
    Configurable parameters:
    1) Timer number;
    2) Channel of the timer;
    3) Duty ratio: from 0x000 to 0xFFFF;
*/
int setPwmDutyRatio(int timer_number, int channel_number, int duty_ratio);

/* PWM get duty ratio (Q) function:
    Configurable parameters:
    1) Timer number;
    2) Channel of the timer;
*/
int getPwmDutyRatio(int timer_number, int channel_number);

/* Timer initialization function.

Native frequency is 8 MHz = 125 nS.
PLEASE NOTE: this is only standard timer initialization.

Input parameters:
  - Timer number
  - Init counter
  - Timer prescaler
  - Timer period

*/
int initTimer (int timer_number, int init_counter, int prescaler, int period);

/* 
    LCD initialization function
*/
int lcdInit(void);

/* 
    ASCII table function for LCD symbols
*/
uint8_t* ascii(char symbol);
